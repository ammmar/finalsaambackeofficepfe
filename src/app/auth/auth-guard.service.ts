import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {AuthService} from "./auth.service";
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  test:any;
  constructor( private router: Router) {
  }

  canActivate(): boolean {
    let user=JSON.parse(localStorage.getItem("currentUser"));
    if (!user) {
      this.router.navigate(['/auth/login']);
      return false;
    }

    return true;

  }
}
