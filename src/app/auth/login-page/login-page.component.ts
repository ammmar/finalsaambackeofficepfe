import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Route, Router} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginForm:FormGroup;
  private errors: string[];
  constructor(private  _fb:FormBuilder,private authService:AuthService
  ,
              private router:Router) {
    this.loginForm=this._fb.group({
      email:["",Validators.compose([Validators.required])],
      password: ["",Validators.compose([Validators.minLength(8),Validators.required])]
  })



  }
login(){
    if(this.loginForm.valid){
      console.log(this.loginForm.value);
this.authService.login(this.loginForm.controls["email"].value,this.loginForm.controls["password"].value).subscribe(res=>{
console.log(res);
  localStorage.setItem('currentUser', JSON.stringify(res));


  this.router.navigate(["/dashboard"]);

  console.log(res);

})

    }
    else {
      this.errors=["not valid"]
    }


}
  ngOnInit() {
  }

}
