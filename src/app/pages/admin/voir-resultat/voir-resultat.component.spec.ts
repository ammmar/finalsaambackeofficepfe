import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirResultatComponent } from './voir-resultat.component';

describe('VoirResultatComponent', () => {
  let component: VoirResultatComponent;
  let fixture: ComponentFixture<VoirResultatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoirResultatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoirResultatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
