import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {CandidatService} from '../../../services/candidat.service';

export const formErrors: { [key: string]: string } = {
  required: 'champ obligatoire',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Le mot de passe doit contenir au moins 8 caractères.\n.',
  minLengthPhone: 'Le numéro de téléphone doit contenir au moins 8 caractères.\n.',
  email:'format email est invalid \n',
  mismatch: 'Les mots de passe ne correspondent pas\n.',
  unique: 'Les mots de passe doivent contenir au moins 3 caractères uniques.\n.'
};
@Component({
  selector: 'app-add-candidat',
  templateUrl: './add-candidat.component.html',
  styleUrls: ['./add-candidat.component.scss']
})
export class AddCandidatComponent implements OnInit {
addCandidat:FormGroup;
  redirectDelay: number = 0;

  errors: string[] = [];
  messages: string[] = [];
  user: any = {rememberMe: true};
  today=new Date();

  showMessages: any = {};
  submitted: boolean = false;
  currentMonth;

  validation = {};
  formErrors = formErrors;

  nom = new FormControl('', [Validators.required]);
  prenom = new FormControl('', [Validators.required]);
  dateDebut = new FormControl('', [Validators.required]);

  userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    niveau : new FormControl('', [Validators.required,Validators.min(0),
      Validators.max(2)]),
     tel : new FormControl('', [Validators.required,Validators.minLength(8),
      Validators.maxLength(8)]),
    experience : new FormControl('', [Validators.required,Validators.minLength(8),
      Validators.maxLength(8)])




});

  ecole = new FormControl('', [Validators.required]);
  experience = new FormControl('', [Validators.required]);
  profil = new FormControl('', [Validators.required]);
  dateDebot = new FormControl('', [Validators.required]);


  constructor(private  candidatService:CandidatService,private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder) {
    let date: Date = new Date();


    let month : number = date.getMonth()+1;

    this.afficheMsq("current month "+this.getCurrentMonth(month)+"           date "+date);

  }


  getCurrentMonth(month:number):string
  {
    switch (month) {
      case 1:
      return "Janvier"
        console.log("It is a janvier.");
        break;
      case 2:
        return "Fevrier";
        console.log("It is a Fevrier.");
        break;
      case 3:
        return "Mars";
        console.log("It is a Tuesday.");
        break;
      case 4:
        return "Avril";
        console.log("It is a Wednesday.");
        break;
      case 5:
        return "Mai";
        console.log("It is a Mai.");
        break;
      case 6:
        return "Juin";

        console.log("It is a Juin.");
        break;
      case 7:
        return "Juillet";
        console.log("It is a Saturday.");
        break;
      case 8:
        return "aout";
        console.log("It is a Saturday.");
        break;
      case 9:
        return "septembre";
        console.log("It is a Saturday.");
        break;
      case 10:
        return "octobre";
        console.log("It is a Saturday.");
        break;
      case 11:
        return "novembre";
        console.log("It is a Saturday.");
        break;
      case 12:
        return "décembre";
        console.log("It is a Saturday.");
        break;
      default:
        console.log("No such day exists!");
        break;
    }
  }

  ngOnInit() {


    this.addCandidat=this.fb.group({
      nom:['',Validators.required],
      prenom:['',Validators.required],
      email:['',Validators.email],
      ecole:['',Validators.required],
      tel:['',Validators.required],
      experience:['',Validators.required],
      niveau:['',Validators.required,,Validators.minLength(0),
        Validators.maxLength(2)],
      profil:['',Validators.required],
      dateDebot:['',Validators.required]
    });
  }

  onSubmit(){
    if(this.addCandidat.valid)
    {
      this.afficheMsq("Valid form "+this.addCandidat.value["nom"]);
      let date=new Date();
      let month : number = date.getMonth()+1;
      this.addCandidat.value["month"]=this.getCurrentMonth(month);
      this.candidatService.creerCandidat(this.addCandidat.value).toPromise().then(response=>{
        this.afficheMsq("add candiat "+JSON.stringify(response));
        Swal.fire({
          title: 'add candidat success  ',
          text: 'success',
          icon: 'success',
        });
        this.router.navigate(['/candidat/']);
      },error=>{
        Swal.fire({
          title: 'add candidat failure ',
          text: 'probleme d ajout candidat',
          icon: 'warning',
        });
        console.log("addTest:Erreur is =========>***"+JSON.stringify(error));
      });
    }
  }
  NavToList(){
    this.router.navigate(['/candidat/']);

  }
  OnReset(){
    this.addCandidat.reset();
  }

  afficheMsq(msg:any)
  {
    console.log("add-candiat      "+msg);
  }

}
