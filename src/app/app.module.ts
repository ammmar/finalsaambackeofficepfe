import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule, ToastrService } from "ngx-toastr";

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import {AppRoutes} from './app.routing';
//Tutorial
//https://sweetalert2.github.io/

//chat tutourial  https://www.yamicode.com/snippets/real-time-chat-angular-spring-boot-java-websocket-stompjs

import {
  NbDialogModule,
   NbDialogService,
 } from '@nebular/theme';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { LoginPageComponent } from './auth/login-page/login-page.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {BasicAuthInterceptor} from './_helpers/basic-auth.interceptor';
import {AuthService} from './auth/auth.service';



@NgModule({
  declarations: [
    AppComponent,LoginPageComponent,
    AdminLayoutComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    ChartsModule,
    RouterModule.forRoot(AppRoutes),
    SidebarModule,
    NbDialogModule.forRoot(),
    NavbarModule,
    ToastrModule.forRoot(),
    FooterModule,
    FixedPluginModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule
  ],
  providers:[NbDialogService,ToastrService,AuthGuardService,AuthService],


    bootstrap: [AppComponent]
})
export class AppModule { }
